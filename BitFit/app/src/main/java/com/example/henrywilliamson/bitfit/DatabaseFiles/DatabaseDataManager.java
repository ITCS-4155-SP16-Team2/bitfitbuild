package com.example.henrywilliamson.bitfit.DatabaseFiles;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.henrywilliamson.bitfit.FitbitClasses.UserValues;

import java.util.List;

public class DatabaseDataManager {
    private SQLiteDatabase db;
    private UserDAO userDAO;

    public DatabaseDataManager(Context mContext) {
        DatabaseOpenHelper dbOpenHelper = new DatabaseOpenHelper(mContext);
        db = dbOpenHelper.getWritableDatabase();
        userDAO = new UserDAO(db);
    }

    public void close() {
        if (db != null)
            db.close();
    }

    public long saveUser(UserValues user) {
        return this.userDAO.save(user);
    }

    public boolean updateUser(UserValues user) {
        return this.userDAO.update(user);
    }

    public List<UserValues> getAllUsers() {
        return this.userDAO.getAll();
    }
}
