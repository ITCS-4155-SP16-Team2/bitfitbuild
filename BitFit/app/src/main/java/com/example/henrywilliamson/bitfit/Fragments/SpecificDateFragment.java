package com.example.henrywilliamson.bitfit.Fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.henrywilliamson.bitfit.Activities.HomepageActivity;
import com.example.henrywilliamson.bitfit.FitbitAPI.FitBitEndpointInterface;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyActivity;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyActivityValues;
import com.example.henrywilliamson.bitfit.R;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class SpecificDateFragment extends Fragment {
    private FitBitEndpointInterface apiService;
    private String date;

    public SpecificDateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_specific_date, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(" View Specific Date");

        initRetrofit();
        final String fullAuthToken = getArguments().getString("fullAuthToken");

        selectDate(fullAuthToken);

        getActivity().findViewById(R.id.selectAnotherDateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate(fullAuthToken);
            }
        });
    }

    private void selectDate(final String fullAuthToken) {
        final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog deptDatePicker = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date = dateFormatter.format(newDate.getTime());

                apiService.getDailyActivity("1/user/-/activities/date/" + date + ".json", fullAuthToken).enqueue(new Callback<DailyActivity>() {
                    @Override
                    public void onResponse(retrofit.Response<DailyActivity> response, Retrofit retrofit) {
                        int steps, floors, calories, activeMinutes;
                        double distance;

                        DailyActivity dailyActivity = new DailyActivity();
                        dailyActivity.setDailyActivity(response.body().getDailyActivity());
                        activeMinutes = Integer.parseInt(dailyActivity.getDailyActivity().getFairlyActiveMinutes());
                        activeMinutes += Integer.parseInt(dailyActivity.getDailyActivity().getVeryActiveMinutes());
                        steps = Integer.parseInt(dailyActivity.getDailyActivity().getSteps());
                        try {
                            floors = Integer.parseInt(dailyActivity.getDailyActivity().getFloors());
                        } catch (NumberFormatException nfe) {
                            floors = 0;
                        }
                        calories = Integer.parseInt(dailyActivity.getDailyActivity().getCaloriesOut());
                        DailyActivityValues dailyActivityValues = new DailyActivityValues();
                        dailyActivityValues.setDistanceValues(dailyActivity.getDailyActivity().getDistanceValues());
                        distance = Double.parseDouble(dailyActivityValues.getDistanceValues().get(0).getDistance()) * 0.621371;

                        String newDateString = date;
                        try {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                            Date d = sdf.parse(date);
                            sdf.applyPattern("EEE, MMM d, yyyy");
                            newDateString = sdf.format(d);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        TextView textView = (TextView) getActivity().findViewById(R.id.selectedDate);
                        textView.setText(newDateString);
                        textView = (TextView) getActivity().findViewById(R.id.stepAmount);
                        String str = NumberFormat.getNumberInstance(Locale.US).format(steps) + " steps";
                        textView.setText(str);
                        textView = (TextView) getActivity().findViewById(R.id.distanceAmount);
                        str = String.format(Locale.ENGLISH, "%.2f", distance) + " miles";
                        textView.setText(str);
                        textView = (TextView) getActivity().findViewById(R.id.calorieAmount);
                        str = NumberFormat.getNumberInstance(Locale.US).format(calories) + " calories burned";
                        textView.setText(str);
                        textView = (TextView) getActivity().findViewById(R.id.floorAmount);
                        str = NumberFormat.getNumberInstance(Locale.US).format(floors) + " floors";
                        textView.setText(str);
                        textView = (TextView) getActivity().findViewById(R.id.activeMinutes);
                        str = NumberFormat.getNumberInstance(Locale.US).format(activeMinutes) + " active minutes";
                        textView.setText(str);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("demo", "Unable to retrieve date values");
                        t.printStackTrace();
                    }
                });
            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        deptDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
        deptDatePicker.show();
    }

    public void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HomepageActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(FitBitEndpointInterface.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
