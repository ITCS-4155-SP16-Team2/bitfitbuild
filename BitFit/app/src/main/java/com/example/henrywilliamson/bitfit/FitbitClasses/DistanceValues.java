package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DistanceValues {

    @SerializedName("distance")
    @Expose
    String distance;

    public String getDistance() {
        return distance;
    }
}
