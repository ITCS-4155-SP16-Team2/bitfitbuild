package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyGoal {

    @SerializedName("goals")
    @Expose
    private DailyGoalValues dailyGoalValues = new DailyGoalValues();

    public DailyGoal() {
        this.dailyGoalValues = new DailyGoalValues();
    }

    public DailyGoalValues getDailyGoals() {
        return dailyGoalValues;
    }

    public void setDailyGoals(DailyGoalValues dailyGoalValues) {
        this.dailyGoalValues = dailyGoalValues;
    }
}