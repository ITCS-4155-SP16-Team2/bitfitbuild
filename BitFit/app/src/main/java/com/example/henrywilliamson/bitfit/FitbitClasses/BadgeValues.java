package com.example.henrywilliamson.bitfit.FitbitClasses;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BadgeValues implements Parcelable {
    @SerializedName("shortName")
    @Expose
    String name;

    @SerializedName("description")
    @Expose
    String description;

    @SerializedName("image100px")
    @Expose
    String icon;

    @SerializedName("timesAchieved")
    @Expose
    String amountAchieved;

    public static final Creator<BadgeValues> CREATOR = new Creator<BadgeValues>() {
        public BadgeValues createFromParcel(Parcel in) {
            return new BadgeValues(in);
        }

        public BadgeValues[] newArray(int size) {
            return new BadgeValues[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(icon);
        dest.writeString(description);
        dest.writeString(amountAchieved);
    }

    private BadgeValues(Parcel in) {
        this.name = in.readString();
        this.icon = in.readString();
        this.description = in.readString();
        this.amountAchieved = in.readString();
    }

    public BadgeValues() {
        this.name = null;
        this.icon = null;
        this.description = null;
        this.amountAchieved = null;
    }

    public String getAmountAchieved() {
        return amountAchieved;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
