package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("user")
    @Expose
    private UserValues user = new UserValues();

    public User() {
        this.user = new UserValues();
    }

    public UserValues getProfile() {
        return user;
    }

    public void setProfile(UserValues user) {
        this.user = user;
    }
}