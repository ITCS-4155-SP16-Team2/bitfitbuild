package com.example.henrywilliamson.bitfit.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.henrywilliamson.bitfit.FitbitAPI.FitBitEndpointInterface;
import com.example.henrywilliamson.bitfit.FitbitAPI.LoginFragment;
import com.example.henrywilliamson.bitfit.FitbitAPI.QuickPreferences;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyActivity;
import com.example.henrywilliamson.bitfit.Fragments.BadgesFragment;
import com.example.henrywilliamson.bitfit.Fragments.DashboardFragment;
import com.example.henrywilliamson.bitfit.Fragments.GameInformationFragment;
import com.example.henrywilliamson.bitfit.Fragments.ProfileFragment;
import com.example.henrywilliamson.bitfit.Fragments.SpecificDateFragment;
import com.example.henrywilliamson.bitfit.R;
import com.squareup.picasso.Picasso;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class HomepageActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DashboardFragment.OnFragmentInteractionListener {
    private String fullAuthToken, todaysSteps = "0";
    private int encodedUserIDIndex;
    private AlertDialog noConnectionAlert;
    public static final String BASE_URL = "https://api.fitbit.com/";
    private FitBitEndpointInterface apiService;
    private View navView;
    private String fragmentTag;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setIcon(R.drawable.bitfit_icon);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
            navView = navigationView.getHeaderView(0);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You do not appear to have access to the internet, please exit the application.");
        builder.setPositiveButton(R.string.exit_application, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        noConnectionAlert = builder.create();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (isNetworkAvailable(getBaseContext())) {
            if (!sharedPreferences.getBoolean(QuickPreferences.HAVE_AUTHORIZATION, false)) { //No token, returns a boolean value
                fragmentTag = "login";
                getFragmentManager().beginTransaction()
                        .add(R.id.container, new LoginFragment(), fragmentTag)
                        .commit();
            } else {
                fullAuthToken = sharedPreferences.getString(QuickPreferences.FULL_AUTHORIZATION, null);
                initRetrofit();

                DashboardFragment dashboardFragment = new DashboardFragment();
                Bundle bundle = new Bundle();
                bundle.putString("fullAuthToken", fullAuthToken);
                dashboardFragment.setArguments(bundle);
                fragmentTag = "dashboard";
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.container, dashboardFragment, fragmentTag)
                        .commit();
            }
        } else
            noConnectionAlert.show();
    }

    public void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(FitBitEndpointInterface.class);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null && drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        else
            super.onBackPressed();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (isNetworkAvailable(getBaseContext())) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment currentFragment = fragmentManager.findFragmentByTag(fragmentTag);
            switch (id) {
                case R.id.nav_view_dashboard:
                    fragmentTag = "dashboard";
                    if (!fragmentTag.equals(currentFragment.getTag())) {
                        DashboardFragment dashboardFragment = new DashboardFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("fullAuthToken", fullAuthToken);
                        dashboardFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, dashboardFragment, fragmentTag)
                                .commit();
                    }
                    break;
                case R.id.nav_view_profile:
                    fragmentTag = "profile";
                    if (!fragmentTag.equals(currentFragment.getTag())) {
                        ProfileFragment profileFragment = new ProfileFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("fullAuthToken", fullAuthToken);
                        profileFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, profileFragment, fragmentTag)
                                .commit();
                    }
                    break;
                case R.id.nav_view_badges:
                    fragmentTag = "badges";
                    if (!fragmentTag.equals(currentFragment.getTag())) {
                        BadgesFragment badgesFragment = new BadgesFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("fullAuthToken", fullAuthToken);
                        bundle.putInt("UserIDIndex", encodedUserIDIndex);
                        badgesFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, badgesFragment, fragmentTag)
                                .commit();
                    }
                    break;
                case R.id.nav_view_date:
                    fragmentTag = "specificdate";
                    if (!fragmentTag.equals(currentFragment.getTag())) {
                        SpecificDateFragment specificDateFragment = new SpecificDateFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("UserIDIndex", encodedUserIDIndex);
                        bundle.putString("fullAuthToken", fullAuthToken);
                        specificDateFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, specificDateFragment, fragmentTag)
                                .commit();
                    }
                    break;
                case R.id.nav_play_games:
                    Intent intent = new Intent(this, UnityPlayerActivity.class);
                    intent.putExtra("STEPS", todaysSteps);
                    intent.putExtra("UserIDIndex", encodedUserIDIndex);
                    startActivity(intent);
                    break;
                case R.id.nav_game_information:
                    fragmentTag = "gameinfo";
                    if (!fragmentTag.equals(currentFragment.getTag())) {
                        GameInformationFragment gameInformationFragment = new GameInformationFragment();
                        Bundle bundle = new Bundle();
                        bundle.putInt("UserIDIndex", encodedUserIDIndex);
                        gameInformationFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container, gameInformationFragment, fragmentTag)
                                .commit();
                    }
                    break;
            }
        } else
            noConnectionAlert.show();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer != null) {
            drawer.closeDrawer(GravityCompat.START);
        }
        return true;
    }

    private void getTodaysSteps() {
        apiService.getDailyActivity("1/user/-/activities/date/today.json", fullAuthToken).enqueue(new Callback<DailyActivity>() {
            @Override
            public void onResponse(retrofit.Response<DailyActivity> response, Retrofit retrofit) {
                DailyActivity dailyActivity = new DailyActivity();
                dailyActivity.setDailyActivity(response.body().getDailyActivity());
                todaysSteps = dailyActivity.getDailyActivity().getSteps();
                String todaysStepsStr = "Todays Steps: " + todaysSteps;
                TextView textView = (TextView) navView.findViewById(R.id.headerTodaysSteps);
                textView.setText(todaysStepsStr);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("demo", "Unable to retrieve todays steps");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void sendProfileData(int encodedUserIDIndex, String avatar, String fullName) {
        this.encodedUserIDIndex = encodedUserIDIndex;
        if (navView != null) {
            try {
                ImageView headerProfilePicture = (ImageView) navView.findViewById(R.id.headerProfilePicture);
                Picasso.with(this).load(avatar).into(headerProfilePicture);
            } catch (NullPointerException npe) {
                Log.d("demo", "No image available");
            }
            TextView textView = (TextView) navView.findViewById(R.id.headerProfileName);
            textView.setText(fullName);
            getTodaysSteps();
        }
    }

    @NonNull
    @Override
    public MenuInflater getMenuInflater() {
        return super.getMenuInflater();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.logOutMenuItem) {
            sharedPreferences.edit().putBoolean(QuickPreferences.HAVE_AUTHORIZATION, false).apply();
            fragmentTag = "login";
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new LoginFragment(), fragmentTag)
                    .commit();
            return true;
        } else
            finish();

        return super.onOptionsItemSelected(item);
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
}
