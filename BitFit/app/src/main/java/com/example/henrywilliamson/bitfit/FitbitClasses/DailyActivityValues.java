package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DailyActivityValues {
    @SerializedName("fairlyActiveMinutes")
    @Expose
    String fairlyActiveMinutes;

    @SerializedName("veryActiveMinutes")
    @Expose
    String veryActiveMinutes;

    @SerializedName("steps")
    @Expose
    String steps;

    @SerializedName("floors")
    @Expose
    String floors;

    @SerializedName("caloriesOut")
    @Expose
    String caloriesOut;

    @SerializedName("distances")
    @Expose
    private List<DistanceValues> distanceValues = new ArrayList<>();

    public DailyActivityValues() {
        this.distanceValues = new ArrayList<>();
    }

    public List<DistanceValues> getDistanceValues() {
        return distanceValues;
    }

    public void setDistanceValues(List<DistanceValues> distanceValues) {
        this.distanceValues = distanceValues;
    }

    public String getCaloriesOut() {
        return caloriesOut;
    }

    public String getFairlyActiveMinutes() {
        return fairlyActiveMinutes;
    }

    public String getFloors() {
        return floors;
    }

    public String getSteps() {
        return steps;
    }

    public String getVeryActiveMinutes() {
        return veryActiveMinutes;
    }
}