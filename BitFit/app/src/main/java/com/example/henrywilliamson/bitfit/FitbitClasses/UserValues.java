package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserValues {
    private long _id;
    private int currency, lastSyncedSteps;
    private String lastSynced, cavemanHighscore, dogParkHighscore, hoopsHighscore, cavemanPowerups; //Not pulled from Fitbit API

    //Following values are pulled from the Fitbit API using Google gson, says they are not used, but they are.
    @SerializedName("avatar150")
    @Expose
    private String avatar150;

    @SerializedName("fullName")
    @Expose
    private String fullName;

    @SerializedName("displayName")
    @Expose
    private String displayName;

    @SerializedName("age")
    @Expose
    private String age;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("height")
    @Expose
    private String height;

    @SerializedName("averageDailySteps")
    @Expose
    private String averageDailySteps;

    @SerializedName("memberSince")
    @Expose
    private String memberSince;

    @SerializedName("encodedId")
    @Expose
    private String encodedUserID;

    public UserValues(String encodedUserID, String lastSynced, int lastSyncedSteps, int currency, String cavemanHighscore, String dogParkHighscore, String hoopsHighscore, String cavemanPowerups) {
        this.encodedUserID = encodedUserID;
        this.lastSynced = lastSynced;
        this.currency = currency;
        this.cavemanHighscore = cavemanHighscore;
        this.dogParkHighscore = dogParkHighscore;
        this.hoopsHighscore = hoopsHighscore;
        this.cavemanPowerups = cavemanPowerups;
        this.lastSyncedSteps = lastSyncedSteps;
    }

    public UserValues() {
    }

    public long get_id() {
        return _id;
    }

    public String getLastSynced() {
        return lastSynced;
    }

    public void setLastSynced(String lastSynced) {
        this.lastSynced = lastSynced;
    }

    public int getLastSyncedSteps() {
        return lastSyncedSteps;
    }

    public void setLastSyncedSteps(int lastSyncedSteps) {
        this.lastSyncedSteps = lastSyncedSteps;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getCurrency() {
        return currency;
    }

    public void setCurrency(int currency) {
        this.currency = currency;
    }

    public String getCavemanHighscore() {
        return cavemanHighscore;
    }

    public void setCavemanHighscore(String cavemanHighscore) {
        this.cavemanHighscore = cavemanHighscore;
    }

    public String getDogParkHighscore() {
        return dogParkHighscore;
    }

    public void setDogParkHighscore(String dogParkHighscore) {
        this.dogParkHighscore = dogParkHighscore;
    }

    public String getHoopsHighscore() {
        return hoopsHighscore;
    }

    public void setHoopsHighscore(String hoopsHighscore) {
        this.hoopsHighscore = hoopsHighscore;
    }

    public String getEncodedUserID() {
        return encodedUserID;
    }

    public void setEncodedUserID(String encodedUserID) {
        this.encodedUserID = encodedUserID;
    }

    public String getAge() {
        return age;
    }

    public String getAvatar150() {
        return avatar150;
    }

    public String getAverageDailySteps() {
        return averageDailySteps;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getFullName() {
        return fullName;
    }

    public String getGender() {
        return gender;
    }

    public String getHeight() {
        return height;
    }

    public String getMemberSince() {
        return memberSince;
    }

    public String getCavemanPowerups() {
        return cavemanPowerups;
    }

    public void setCavemanPowerups(String cavemanPowerups) {
        this.cavemanPowerups = cavemanPowerups;
    }
}
