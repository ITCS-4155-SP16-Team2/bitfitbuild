package com.example.henrywilliamson.bitfit.DatabaseFiles;

import android.database.sqlite.SQLiteDatabase;

public class UserTable {

    static final String TABLENAME = "users";
    static final String COLUMN_ID = "_id";
    static final String COLUMN_ENCODED_USERID = "user_id";
    static final String COLUMN_GAME_LAST_SYNCED = "last_synced";
    static final String COLUMN_GAME_LAST_SYNCED_STEPS = "last_synced_steps";
    static final String COLUMN_GAME_CURRENCY = "currency";
    static final String COLUMN_CAVEMAN_HIGHSCORE = "caveman_highscore";
    static final String COLUMN_DOGPARK_HIGHSCORE = "dogpark_highscore";
    static final String COLUMN_HOOPS_HIGHSCORE = "hoops_highscore";
    static final String COLUMN_CAVEMAN_POWERUPS = "caveman_powerups";

    static public void onCreate(SQLiteDatabase db) {
        String sb = ("CREATE TABLE " + TABLENAME + " (") +
                COLUMN_ID + " integer primary key autoincrement, " +
                COLUMN_ENCODED_USERID + " text not null, " +
                COLUMN_GAME_LAST_SYNCED + " text not null, " +
                COLUMN_GAME_LAST_SYNCED_STEPS + " text not null, " +
                COLUMN_GAME_CURRENCY + " text not null, " +
                COLUMN_CAVEMAN_HIGHSCORE + " text not null, " +
                COLUMN_DOGPARK_HIGHSCORE + " text not null, " +
                COLUMN_HOOPS_HIGHSCORE + " text not null, " +
                COLUMN_CAVEMAN_POWERUPS + " text not null);";
        db.execSQL(sb);
    }

    static public void onUpgrade(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLENAME);
        UserTable.onCreate(db);
    }
}
