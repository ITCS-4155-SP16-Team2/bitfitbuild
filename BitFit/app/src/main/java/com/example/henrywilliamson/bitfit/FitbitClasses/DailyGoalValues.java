package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyGoalValues {
    @SerializedName("activeMinutes")
    @Expose
    String activeMinutesGoal;

    @SerializedName("caloriesOut")
    @Expose
    String caloriesOutGoal;

    @SerializedName("distance")
    @Expose
    String distanceGoal;

    @SerializedName("floors")
    @Expose
    String floorsGoal;

    @SerializedName("steps")
    @Expose
    String stepsGoal;

    public String getActiveMinutesGoal() {
        return activeMinutesGoal;
    }

    public String getDistanceGoal() {
        return distanceGoal;
    }

    public String getFloorsGoal() {
        return floorsGoal;
    }

    public String getStepsGoal() {
        return stepsGoal;
    }

    public String getCaloriesOutGoal() {
        return caloriesOutGoal;
    }
}