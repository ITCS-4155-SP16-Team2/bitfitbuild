package com.example.henrywilliamson.bitfit.FitbitAPI;

import com.example.henrywilliamson.bitfit.FitbitClasses.Badge;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyActivity;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyGoal;
import com.example.henrywilliamson.bitfit.FitbitClasses.User;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Url;

public interface FitBitEndpointInterface {

    @GET("1/user/-/profile.json")
    Call<User> getProfile(@Header("Authorization") String accessToken);

    @GET("1/user/-/activities/goals/daily.json")
    Call<DailyGoal> getDailyGoals(@Header("Authorization") String accessToken);

    @GET("1/user/-/badges.json")
    Call<Badge> getBadges(@Header("Authorization") String accessToken);

    @GET
    Call<DailyActivity> getDailyActivity(@Url String url, @Header("Authorization") String accessToken);
}
