package com.example.henrywilliamson.bitfit.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.henrywilliamson.bitfit.DatabaseFiles.DatabaseDataManager;
import com.example.henrywilliamson.bitfit.R;

public class GameInformationFragment extends Fragment {
    private int userIDIndex;

    public GameInformationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_game_information, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(" Game Information");
        userIDIndex = getArguments().getInt("UserIDIndex");
        getData(userIDIndex);
    }

    public void getData(int userIDIndex) {
        DatabaseDataManager dm = new DatabaseDataManager(getActivity());
        TextView textView = (TextView) getActivity().findViewById(R.id.cavemanRunHighscore);
        String str = "Caveman Run: " + dm.getAllUsers().get(userIDIndex).getCavemanHighscore();
        textView.setText(str);
        textView = (TextView) getActivity().findViewById(R.id.dogParkHighscore);
        str = "Dog Park: " + dm.getAllUsers().get(userIDIndex).getDogParkHighscore();
        textView.setText(str);
        textView = (TextView) getActivity().findViewById(R.id.hoopsHighscore);
        str = "Hoops: " + dm.getAllUsers().get(userIDIndex).getHoopsHighscore();
        textView.setText(str);
        dm.close();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();
        getData(userIDIndex); //so the highscores get updated after closing game
    }
}
