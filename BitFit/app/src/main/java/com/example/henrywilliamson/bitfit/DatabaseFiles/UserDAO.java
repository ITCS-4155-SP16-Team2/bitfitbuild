package com.example.henrywilliamson.bitfit.DatabaseFiles;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.henrywilliamson.bitfit.FitbitClasses.UserValues;

import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private SQLiteDatabase db;

    public UserDAO(SQLiteDatabase db) {
        this.db = db;
    }

    public long save(UserValues user) {
        ContentValues values = new ContentValues();
        values.put(UserTable.COLUMN_ENCODED_USERID, user.getEncodedUserID());
        values.put(UserTable.COLUMN_GAME_LAST_SYNCED, user.getLastSynced());
        values.put(UserTable.COLUMN_GAME_LAST_SYNCED_STEPS, user.getLastSyncedSteps());
        values.put(UserTable.COLUMN_GAME_CURRENCY, user.getCurrency());
        values.put(UserTable.COLUMN_CAVEMAN_HIGHSCORE, user.getCavemanHighscore());
        values.put(UserTable.COLUMN_DOGPARK_HIGHSCORE, user.getDogParkHighscore());
        values.put(UserTable.COLUMN_HOOPS_HIGHSCORE, user.getHoopsHighscore());
        values.put(UserTable.COLUMN_CAVEMAN_POWERUPS, user.getCavemanPowerups());
        return db.insert(UserTable.TABLENAME, null, values);
    }

    public boolean update(UserValues user) {
        ContentValues values = new ContentValues();
        values.put(UserTable.COLUMN_ENCODED_USERID, user.getEncodedUserID());
        values.put(UserTable.COLUMN_GAME_LAST_SYNCED, user.getLastSynced());
        values.put(UserTable.COLUMN_GAME_LAST_SYNCED_STEPS, user.getLastSyncedSteps());
        values.put(UserTable.COLUMN_GAME_CURRENCY, user.getCurrency());
        values.put(UserTable.COLUMN_CAVEMAN_HIGHSCORE, user.getCavemanHighscore());
        values.put(UserTable.COLUMN_DOGPARK_HIGHSCORE, user.getDogParkHighscore());
        values.put(UserTable.COLUMN_HOOPS_HIGHSCORE, user.getHoopsHighscore());
        values.put(UserTable.COLUMN_CAVEMAN_POWERUPS, user.getCavemanPowerups());
        return db.update(UserTable.TABLENAME, values, UserTable.COLUMN_ID + "=?", new String[]{user.get_id() + ""}) > 0;
    }

    public List<UserValues> getAll() {
        List<UserValues> users = new ArrayList<>();
        Cursor c = db.query(UserTable.TABLENAME, new String[]{UserTable.COLUMN_ID, UserTable.COLUMN_ENCODED_USERID, UserTable.COLUMN_GAME_LAST_SYNCED, UserTable.COLUMN_GAME_LAST_SYNCED_STEPS, UserTable.COLUMN_GAME_CURRENCY, UserTable.COLUMN_CAVEMAN_HIGHSCORE, UserTable.COLUMN_DOGPARK_HIGHSCORE, UserTable.COLUMN_HOOPS_HIGHSCORE, UserTable.COLUMN_CAVEMAN_POWERUPS}, null, null, null, null, null);
        if (c != null && c.moveToFirst()) {
            do {
                UserValues user = buildUserFromCursor(c);
                if (user != null)
                    users.add(user);
            } while (c.moveToNext());

            if (!c.isClosed())
                c.close();
        }
        return users;
    }

    private UserValues buildUserFromCursor(Cursor c) {
        UserValues user = new UserValues();
        user.set_id(c.getLong(0));
        user.setEncodedUserID(c.getString(1));
        user.setLastSynced(c.getString(2));
        user.setLastSyncedSteps(c.getInt(3));
        user.setCurrency(c.getInt(4));
        user.setCavemanHighscore(c.getString(5));
        user.setDogParkHighscore(c.getString(6));
        user.setHoopsHighscore(c.getString(7));
        user.setCavemanPowerups(c.getString(8));
        return user;
    }
}
