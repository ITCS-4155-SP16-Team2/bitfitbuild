package com.example.henrywilliamson.bitfit.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.henrywilliamson.bitfit.Activities.HomepageActivity;
import com.example.henrywilliamson.bitfit.FitbitAPI.FitBitEndpointInterface;
import com.example.henrywilliamson.bitfit.FitbitClasses.User;
import com.example.henrywilliamson.bitfit.R;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class ProfileFragment extends Fragment {
    private FitBitEndpointInterface apiService;
    SwipeRefreshLayout swipeRefreshLayout;
    private String age, gender, height, avatar, fullName, averageDailySteps, joinedDate;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_profile, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(" Your Profile");

        initRetrofit();
        final String fullAuthToken = getArguments().getString("fullAuthToken");

        getActivity().findViewById(R.id.edit_profile_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.fitbit.com/user/profile/edit")));
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(R.id.profile_swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getProfile(fullAuthToken);
            }
        });

        getProfile(fullAuthToken);
    }

    public void getProfile(String fullAuthToken) {
        apiService.getProfile(fullAuthToken).enqueue(new Callback<User>() {
            @Override
            public void onResponse(retrofit.Response<User> response, Retrofit retrofit) {
                User profile = new User();
                profile.setProfile(response.body().getProfile());
                avatar = profile.getProfile().getAvatar150();
                fullName = profile.getProfile().getFullName();
                age = profile.getProfile().getAge();
                gender = profile.getProfile().getGender();
                height = profile.getProfile().getHeight();
                averageDailySteps = profile.getProfile().getAverageDailySteps();
                joinedDate = profile.getProfile().getMemberSince();

                try {
                    ImageView userIcon = (ImageView) getActivity().findViewById(R.id.userIcon);
                    Picasso.with(getActivity()).load(avatar).into(userIcon);
                } catch (NullPointerException npe) {
                    Log.d("demo", "No image available");
                }

                TextView textView = (TextView) getActivity().findViewById(R.id.fullNameTV);
                textView.setText(fullName);

                textView = (TextView) getActivity().findViewById(R.id.genderTV);
                String changeCase = gender.substring(0, 1) + gender.substring(1).toLowerCase();
                String searchResult = "Gender: " + changeCase;
                textView.setText(searchResult);

                textView = (TextView) getActivity().findViewById(R.id.ageTV);
                searchResult = "Age: " + age;
                textView.setText(searchResult);

                textView = (TextView) getActivity().findViewById(R.id.heightTV);
                double heightFeet = Double.parseDouble(height) * 0.0328084; //convert cm to feet
                String heightFeetVar = Double.toString(heightFeet).substring(0, 1); //store feet value
                double heightInches = (heightFeet - Double.parseDouble(heightFeetVar)) * 12; //store inches variable
                searchResult = "Height: " + heightFeetVar + "' " + (int) Math.floor(heightInches + 0.5f); //combine feet and inches(rounded up or down)
                textView.setText(searchResult);

                textView = (TextView) getActivity().findViewById(R.id.avgDailyStepsTV);
                searchResult = "Average daily steps: " + NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(averageDailySteps));
                textView.setText(searchResult);

                String newDateString = joinedDate;
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date d = sdf.parse(joinedDate);
                    sdf.applyPattern("MMM dd, yyyy");
                    newDateString = sdf.format(d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                searchResult = "Joined " + newDateString;
                textView = (TextView) getActivity().findViewById(R.id.joinedDateTV);
                textView.setText(searchResult);
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("demo", "FAIL");
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HomepageActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(FitBitEndpointInterface.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
