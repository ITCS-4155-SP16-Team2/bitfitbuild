package com.example.henrywilliamson.bitfit.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.henrywilliamson.bitfit.Activities.HomepageActivity;
import com.example.henrywilliamson.bitfit.FitbitAPI.FitBitEndpointInterface;
import com.example.henrywilliamson.bitfit.FitbitClasses.Badge;
import com.example.henrywilliamson.bitfit.FitbitClasses.BadgeAdapter;
import com.example.henrywilliamson.bitfit.FitbitClasses.BadgeValues;
import com.example.henrywilliamson.bitfit.R;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class BadgesFragment extends Fragment {
    private FitBitEndpointInterface apiService;
    ArrayList<BadgeValues> badges;

    public BadgesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_badges, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle(" Your Badges");

        initRetrofit();
        String fullAuthToken = getArguments().getString("fullAuthToken");

        badges = new ArrayList<>();
        apiService.getBadges(fullAuthToken).enqueue(new Callback<Badge>() {
            @Override
            public void onResponse(Response<Badge> response, Retrofit retrofit) {
                Badge badge = new Badge();
                badge.setBadgeValues(response.body().getBadgeValues());
                for (BadgeValues badgeValue : badge.getBadgeValues()) {
                    badges.add(badgeValue);
                }
                ListView badgeList = (ListView) getActivity().findViewById(R.id.badgeList);
                badgeList.setAdapter(new BadgeAdapter(getActivity(), R.layout.badge_list_layout, badges));
            }

            @Override
            public void onFailure(Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HomepageActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(FitBitEndpointInterface.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
