package com.example.henrywilliamson.bitfit.FitbitClasses;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.henrywilliamson.bitfit.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BadgeAdapter extends ArrayAdapter<BadgeValues> {
    Activity mContext;
    int mResource;
    ArrayList<BadgeValues> mData;

    public BadgeAdapter(Activity context, int resource, ArrayList<BadgeValues> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.mResource = resource;
        this.mData = objects;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }

        final BadgeValues BadgeItem = mData.get(position);

        TextView textView = (TextView) convertView.findViewById(R.id.badgeName);
        textView.setText(BadgeItem.getName());

        textView = (TextView) convertView.findViewById(R.id.badgeDescription);
        textView.setText(BadgeItem.getDescription());

        textView = (TextView) convertView.findViewById(R.id.badgeAmountEarned);
        String amountAchievedStr = "Earned " + BadgeItem.getAmountAchieved() + " times";
        textView.setText(amountAchievedStr);

        try {
            ImageView imageView = (ImageView) convertView.findViewById(R.id.badgeIcon);
            Picasso.with(mContext).load(BadgeItem.getIcon()).into(imageView);
        } catch (NullPointerException npe) {
            Log.d("demo", "No image available");
        }
        return convertView;
    }
}
