package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Badge {

    @SerializedName("badges")
    @Expose
    private List<BadgeValues> badgeValues = new ArrayList<>();

    public Badge() {
        this.badgeValues = new ArrayList<>();
    }

    public List<BadgeValues> getBadgeValues() {
        return badgeValues;
    }

    public void setBadgeValues(List<BadgeValues> badgeValues) {
        this.badgeValues = badgeValues;
    }
}