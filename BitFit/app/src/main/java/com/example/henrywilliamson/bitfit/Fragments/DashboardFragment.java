package com.example.henrywilliamson.bitfit.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.henrywilliamson.bitfit.Activities.HomepageActivity;
import com.example.henrywilliamson.bitfit.DatabaseFiles.DatabaseDataManager;
import com.example.henrywilliamson.bitfit.FitbitAPI.FitBitEndpointInterface;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyActivity;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyActivityValues;
import com.example.henrywilliamson.bitfit.FitbitClasses.DailyGoal;
import com.example.henrywilliamson.bitfit.FitbitClasses.User;
import com.example.henrywilliamson.bitfit.FitbitClasses.UserValues;
import com.example.henrywilliamson.bitfit.R;

import java.text.NumberFormat;
import java.util.Locale;

import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class DashboardFragment extends Fragment {
    private FitBitEndpointInterface apiService;
    private OnFragmentInteractionListener mListener;
    DatabaseDataManager dm;
    View v;
    SwipeRefreshLayout swipeRefreshLayout;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_dashboard, container, false);

        initRetrofit();
        final String fullAuthToken = getArguments().getString("fullAuthToken");

        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.dashboard_swipe_container);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(fullAuthToken);
            }
        });

        v.findViewById(R.id.log_activities_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.fitbit.com/activities")));
            }
        });

        getData(fullAuthToken);

        return v;
    }


    public void getData(final String fullAuthToken) {
        apiService.getProfile(fullAuthToken).enqueue(new Callback<User>() {
            @Override
            public void onResponse(retrofit.Response<User> response, Retrofit retrofit) {

                String avatar, fullName, displayName, encodedUserID;

                User profile = new User();
                profile.setProfile(response.body().getProfile());
                avatar = profile.getProfile().getAvatar150();
                fullName = profile.getProfile().getFullName();
                displayName = profile.getProfile().getDisplayName();
                encodedUserID = profile.getProfile().getEncodedUserID();

                getActivity().setTitle(" Welcome, " + displayName);
                dm = new DatabaseDataManager(getActivity());
                if (dm.getAllUsers().size() <= 0) { //no users in database
                    dm.saveUser(new UserValues(encodedUserID, "", 0, 0, "0", "0", "0", "NULL"));
                    mListener.sendProfileData(0, avatar, fullName);
                } else {
                    boolean foundUser = false;
                    for (int i = 0; i < dm.getAllUsers().size(); i++) { //searching for user
                        if (dm.getAllUsers().get(i).getEncodedUserID().equals(encodedUserID)) {
                            foundUser = true;
                            mListener.sendProfileData(i, avatar, fullName);
                            break;
                        }
                    }
                    if (!foundUser) { //no user found, so create new one
                        dm.saveUser(new UserValues(encodedUserID, "", 0, 0, "0", "0", "0", "NULL"));
                        mListener.sendProfileData(dm.getAllUsers().size() - 1, avatar, fullName);
                    }
                }
                dm.close();
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("demo", "Unable to send profile data");
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        apiService.getDailyGoals(fullAuthToken).enqueue(new Callback<DailyGoal>() { //Get goals
            @Override
            public void onResponse(retrofit.Response<DailyGoal> response, Retrofit retrofit) {
                int floorsGoal;

                DailyGoal dailyGoal = new DailyGoal();
                dailyGoal.setDailyGoals(response.body().getDailyGoals());
                final int activeMinutesGoal = Integer.parseInt(dailyGoal.getDailyGoals().getActiveMinutesGoal());
                final int caloriesOutGoal = Integer.parseInt(dailyGoal.getDailyGoals().getCaloriesOutGoal());
                final double distanceGoal = Double.parseDouble(dailyGoal.getDailyGoals().getDistanceGoal()) * 0.621371;
                try {
                    floorsGoal = Integer.parseInt(dailyGoal.getDailyGoals().getFloorsGoal());
                } catch (NumberFormatException nfe) {
                    floorsGoal = 0;
                }
                final int finalFloorsGoal = floorsGoal;
                final int stepsGoal = Integer.parseInt(dailyGoal.getDailyGoals().getStepsGoal());

                apiService.getDailyActivity("1/user/-/activities/date/today.json", fullAuthToken).enqueue(new Callback<DailyActivity>() { //get todays activity
                    @Override
                    public void onResponse(retrofit.Response<DailyActivity> response, Retrofit retrofit) {
                        int steps, floors, calories, activeMinutes;
                        double distance;

                        final DailyActivity dailyActivity = new DailyActivity();
                        dailyActivity.setDailyActivity(response.body().getDailyActivity());
                        activeMinutes = Integer.parseInt(dailyActivity.getDailyActivity().getFairlyActiveMinutes());
                        activeMinutes += Integer.parseInt(dailyActivity.getDailyActivity().getVeryActiveMinutes());
                        steps = Integer.parseInt(dailyActivity.getDailyActivity().getSteps());

                        try {
                            floors = Integer.parseInt(dailyActivity.getDailyActivity().getFloors());
                        } catch (NumberFormatException nfe) {
                            floors = 0;
                        }
                        calories = Integer.parseInt(dailyActivity.getDailyActivity().getCaloriesOut());
                        DailyActivityValues dailyActivityValues = new DailyActivityValues();
                        dailyActivityValues.setDistanceValues(dailyActivity.getDailyActivity().getDistanceValues());
                        distance = Double.parseDouble(dailyActivityValues.getDistanceValues().get(0).getDistance()) * 0.621371;

                        TextView textView = (TextView) v.findViewById(R.id.steps);
                        String str = NumberFormat.getNumberInstance(Locale.US).format(steps) + " steps";
                        textView.setText(str);
                        ProgressBar progressBar = (ProgressBar) v.findViewById(R.id.stepsProgressBar);
                        progressBar.setMax(stepsGoal);
                        progressBar.setProgress(steps);
                        textView = (TextView) v.findViewById(R.id.stepsGoal);
                        str = "Goal: " + NumberFormat.getNumberInstance(Locale.US).format(stepsGoal);
                        textView.setText(str);

                        textView = (TextView) v.findViewById(R.id.distance);
                        str = String.format(Locale.ENGLISH, "%.2f", distance) + " miles";
                        textView.setText(str);
                        progressBar = (ProgressBar) v.findViewById(R.id.distanceProgressBar);
                        progressBar.setMax((int) distanceGoal);
                        progressBar.setProgress((int) Math.round(distance));
                        textView = (TextView) v.findViewById(R.id.distanceGoal);
                        str = "Goal: " + String.format(Locale.ENGLISH, "%.2f", distanceGoal);
                        textView.setText(str);

                        textView = (TextView) v.findViewById(R.id.caloriesOut);
                        str = NumberFormat.getNumberInstance(Locale.US).format(calories) + " calories burned";
                        textView.setText(str);
                        progressBar = (ProgressBar) v.findViewById(R.id.caloriesProgressBar);
                        progressBar.setMax(caloriesOutGoal);
                        progressBar.setProgress(calories);
                        textView = (TextView) v.findViewById(R.id.caloriesOutGoal);
                        str = "Goal: " + NumberFormat.getNumberInstance(Locale.US).format(caloriesOutGoal);
                        textView.setText(str);

                        textView = (TextView) v.findViewById(R.id.floors);
                        str = NumberFormat.getNumberInstance(Locale.US).format(floors) + " floors";
                        textView.setText(str);
                        progressBar = (ProgressBar) v.findViewById(R.id.floorsProgressBar);
                        progressBar.setMax(finalFloorsGoal);
                        progressBar.setProgress(floors);
                        textView = (TextView) v.findViewById(R.id.floorsGoal);
                        str = "Goal: " + NumberFormat.getNumberInstance(Locale.US).format(finalFloorsGoal);
                        textView.setText(str);

                        textView = (TextView) v.findViewById(R.id.activeMinutes);
                        str = activeMinutes + " active minutes";
                        textView.setText(str);
                        progressBar = (ProgressBar) v.findViewById(R.id.activeMinutesProgressBar);
                        progressBar.setMax(activeMinutesGoal);
                        progressBar.setProgress(activeMinutes);
                        textView = (TextView) v.findViewById(R.id.activeMinutesGoal);
                        str = "Goal: " + activeMinutesGoal;
                        textView.setText(str);
                        swipeRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        Log.d("demo", "Unable to retrieve daily activity");
                        t.printStackTrace();
                        swipeRefreshLayout.setRefreshing(false);
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("demo", "Unable to retrieve daily activity");
                t.printStackTrace();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    public void initRetrofit() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(HomepageActivity.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = retrofit.create(FitBitEndpointInterface.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener)
            mListener = (OnFragmentInteractionListener) context;
        else
            throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnFragmentInteractionListener {
        void sendProfileData(int encodedUserIDIndex, String avatar, String fullName);
    }
}
