package com.example.henrywilliamson.bitfit.FitbitClasses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DailyActivity {

    @SerializedName("summary")
    @Expose
    private DailyActivityValues dailyActivityValues = new DailyActivityValues();

    public DailyActivity() {
        this.dailyActivityValues = new DailyActivityValues();
    }

    public DailyActivityValues getDailyActivity() {
        return dailyActivityValues;
    }

    public void setDailyActivity(DailyActivityValues dailyActivityValues) {
        this.dailyActivityValues = dailyActivityValues;
    }
}