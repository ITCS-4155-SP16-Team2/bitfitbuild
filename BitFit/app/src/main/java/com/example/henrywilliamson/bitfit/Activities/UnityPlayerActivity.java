package com.example.henrywilliamson.bitfit.Activities;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Window;

import com.example.henrywilliamson.bitfit.DatabaseFiles.DatabaseDataManager;
import com.example.henrywilliamson.bitfit.FitbitClasses.UserValues;
import com.unity3d.player.UnityPlayer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UnityPlayerActivity extends Activity {
    protected UnityPlayer mUnityPlayer; // don't change the name of this variable; referenced from native code
    private DatabaseDataManager dm;
    private String todaysDate;
    private int userIDIndex;
    private static int DAILY_REWARD = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dm = new DatabaseDataManager(this);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        getWindow().setFormat(PixelFormat.RGBX_8888); // <--- This makes xperia play happy

        mUnityPlayer = new UnityPlayer(this);
        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
        Date date = new Date();
        todaysDate = dateFormat.format(date);

        Intent intent = getIntent();
        userIDIndex = intent.getIntExtra("UserIDIndex", -1);
        if (userIDIndex == -1)
            finish();
        String steps = intent.getStringExtra("STEPS");

        if (!dm.getAllUsers().get(userIDIndex).getLastSynced().equals(todaysDate)) {// add currency to existing currency for signing in today
            updateCurrency(dm.getAllUsers().get(userIDIndex).getCurrency() + Integer.parseInt(steps) + DAILY_REWARD); //adds the static daily reward as a login reward
            updateLastSyncedSteps(Integer.parseInt(steps));
            sendDailyRewardToUnity();
            sendCavemanHSToUnity();
            sendCavemanPowerupsToUnity();
            sendDogParkHSToUnity();
            sendHoopsHSToUnity();
        } else if (dm.getAllUsers().get(userIDIndex).getLastSyncedSteps() != Integer.parseInt(steps)) { //user has synced today but has more steps since last synced
            updateCurrency(dm.getAllUsers().get(userIDIndex).getCurrency() + (Integer.parseInt(steps) - dm.getAllUsers().get(userIDIndex).getLastSyncedSteps()));
            updateLastSyncedSteps(Integer.parseInt(steps));
            sendCavemanHSToUnity();
            sendCavemanPowerupsToUnity();
            sendDogParkHSToUnity();
            sendHoopsHSToUnity();
        } else { //User has already synced today and has the same amount of steps
            sendCurrencyToUnity();
            sendCavemanHSToUnity();
            sendCavemanPowerupsToUnity();
            sendDogParkHSToUnity();
            sendHoopsHSToUnity();
        }
    }

    public void updateLastSyncedSteps(int steps) {
        UserValues user = dm.getAllUsers().get(userIDIndex);
        user.setLastSyncedSteps(steps);
        dm.updateUser(user);
    }

    public void sendDailyRewardToUnity() {
        UnityPlayer.UnitySendMessage("TitleAndroidControllerObject", "dailyReward", Integer.toString(DAILY_REWARD));
    }

    public void updateCurrency(int curr) {
        UserValues user = dm.getAllUsers().get(userIDIndex);
        user.setCurrency(curr);
        user.setLastSynced(todaysDate);
        dm.updateUser(user);
        sendCurrencyToUnity();
    }

    public void sendCurrencyToUnity() {
        UnityPlayer.UnitySendMessage("TitleAndroidControllerObject", "HelloAndroid", Integer.toString(dm.getAllUsers().get(userIDIndex).getCurrency()));
    }

    public void updateCavemanHighscore(String score) { //gets called from Unity
        UserValues user = dm.getAllUsers().get(userIDIndex);
        user.setCavemanHighscore(score);
        dm.updateUser(user);
        sendCavemanHSToUnity();
    }

    public void sendCavemanHSToUnity() {
        UnityPlayer.UnitySendMessage("TitleAndroidControllerObject", "cavemanHighscore", dm.getAllUsers().get(userIDIndex).getCavemanHighscore());
    }

    public void updateDogParkHighscore(String score) { //gets called from Unity
        UserValues user = dm.getAllUsers().get(userIDIndex);
        user.setDogParkHighscore(score);
        dm.updateUser(user);
        sendDogParkHSToUnity();
    }

    public void sendDogParkHSToUnity() {
        UnityPlayer.UnitySendMessage("TitleAndroidControllerObject", "dogParkHighscore", dm.getAllUsers().get(userIDIndex).getDogParkHighscore());
    }

    public void updateHoopsHighscore(String score) { //gets called from Unity
        UserValues user = dm.getAllUsers().get(userIDIndex);
        user.setHoopsHighscore(score);
        dm.updateUser(user);
        sendHoopsHSToUnity();
    }

    public void sendHoopsHSToUnity() {
        UnityPlayer.UnitySendMessage("TitleAndroidControllerObject", "hoopsHighScore", dm.getAllUsers().get(userIDIndex).getHoopsHighscore());
    }

    public void updateCavemanPowerups(String str) { //gets called from Unity
        UserValues user = dm.getAllUsers().get(userIDIndex);
        user.setCavemanPowerups(str);
        dm.updateUser(user);
        sendCavemanPowerupsToUnity();
    }

    public void sendCavemanPowerupsToUnity() {
        UnityPlayer.UnitySendMessage("TitleAndroidControllerObject", "CavemanRunPowerUps", dm.getAllUsers().get(userIDIndex).getCavemanPowerups());
    }

    // Quit Unity
    @Override
    protected void onDestroy() {
        mUnityPlayer.quit();
        dm.close();
        super.onDestroy();
    }

    // Pause Unity
    @Override
    protected void onPause() {
        super.onPause();
        mUnityPlayer.pause();
    }

    // Resume Unity
    @Override
    protected void onResume() {
        super.onResume();
        mUnityPlayer.resume();
    }

    // This ensures the layout will be correct.
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mUnityPlayer.configurationChanged(newConfig);
    }

    // Notify Unity of the focus change.
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mUnityPlayer.windowFocusChanged(hasFocus);
    }

    // For some reason the multiple keyevent type is not supported by the ndk.
    // Force event injection by overriding dispatchKeyEvent().
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE)
            return mUnityPlayer.injectEvent(event);
        return super.dispatchKeyEvent(event);
    }

    // Pass any events not handled by (unfocused) views straight to UnityPlayer
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }

    /*API12*/
    public boolean onGenericMotionEvent(MotionEvent event) {
        return mUnityPlayer.injectEvent(event);
    }
}
